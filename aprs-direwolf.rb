#!/usr/bin/env ruby

IO.popen('direwolf -t 0 -q h -q d', 'r').each_line do |lin|
	r = lin.force_encoding('binary').chomp
	next  unless r =~ /^\[[0-9]\.[0-9]\] /

	r = $'
	while x = r[/<0x[0-9a-f]+>/]
		x = '<0x0b>'  if x == '<0x0a>'	# avoid linesplitting
		r = $` + x[3..-2].to_i(16).chr + $'
	end

	puts r
	STDOUT.flush
end

