#!/usr/bin/env perl

# requires CPAN libraries:
#   cpan install Ham::APRS::FAP

# requires aprs-direwolf.rb's stdout

use Ham::APRS::FAP qw(parseaprs distance kiss_to_tnc2);
use POSIX;

# home station position (for distance calculation)
my $latdist=40.6975638;
my $londist=14.4621494;

my $feeder = $ARGV[0];

open(INPUT, "$feeder|");
while (<INPUT>)
{
  my $pkt = $_.chomp;
  my %dat = ();
  my $t = time;
  my $x = parseaprs($pkt, \%dat);

  #my $dbg = strftime('%H:%M:%S ', localtime);
  #print "$dbg\t$pkt\n";

  open(OUT, strftime('>>%Y%m%d-%H.aprs', localtime));
  if($x != 1)
  {
    if($pkt != "")
    {
      print OUT "resultcode\t$dat{resultcode}\n";
      print OUT "resultmsg\t$dat{resultmsg}\n";
      print OUT "aprs_packet\terror\t$t\n\n";
    }
  }
  else
  {
    while(my($k,$v) = each %dat)
    {
      # forget body, origpacket, header - they may have invalid characters
      if($k ne 'body' and $k ne 'origpacket' and $k ne 'header')
      {
        # check arrays
        if($k eq 'capabilities' or $k eq 'wx' or $k eq 'digipeaters' or $k eq 'telemetry')
        {
          # digipeaters require special treatment
          if($k eq 'digipeaters')
          {
            while(my($kk,$vv) = each(%v))
            {
              while(my($kkk,$vvv) = each(%vv))
              {
                print OUT "digi_$kk" . "_$kkk\t$vvv\n";
              }
            }
          }
          else
          {
            # wx and capabilities and telemetry
            while(my($kk,$vv) = each(%v))
            {
              print OUT "$k" . "_$kk\t$vv\n";
            }
          }
        }
        else
        {
          if($k eq 'longitude')
          {
            my $dist = distance($latdist, $londist, $dat{latitude}, $dat{longitude});
            print OUT "distance\t$dist\n";
          }
          # fallback for extra fields:
          print OUT "$k\t$v\n";
        }
      }
    }

    print OUT "aprs_packet\tok\t$t\n\n";
  }

  close(OUT);
}

